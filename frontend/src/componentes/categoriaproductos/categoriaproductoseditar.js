import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function CategoriaProductosEditar()
{
    const parametros = useParams()
    const[nombre, setNombre] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/categoriaproductos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/categoriaproductos/cargar/${parametros.id}`
        /*{
            headers:{'x-auth-token':''}
        }*/
        ).then(res => {
        //axios.post(`/api/categoriaproductos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataCategoriaProductos = res.data[0]
        setNombre(dataCategoriaProductos.nombre)
        setActivo(dataCategoriaProductos.activo)
        })
    }, [])

    function categoriaproductosActualizar()
    {
        const categoriaproductoactualizar = {
            id: parametros.id,
            nombre: nombre,   
            activo: activo
        }

        console.log(categoriaproductoactualizar)
        axios.post(`/api/categoriaproductos/editar/${parametros.id}`,categoriaproductoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/categoriaproductoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function categoriaproductosRegresar()
    {
        //window.location.href="/";
        navigate('/categoriaproductoslistar')
    }


    return(
    
        <div className="container mt-5">
        <h4>Editar Categoría Productos</h4>
        <div className="row">
            <div class="col-sm-12 table-responsive">
                <table className="table table-inverse">
                    <div className="col-sm-12">
                        <form>
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre</label>
                                <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="activo">Estado</label>
                                <input type="text" className="form-control"id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                            </div>
                            <button type="button" className="btn btn-info" onClick={categoriaproductosRegresar}>Regresar</button>
                            <button type="button" className="btn btn-success" onClick={categoriaproductosActualizar}>Actualizar</button>
                        </form>
                    </div>
                </table>
            </div>
        </div>
    </div>  
   
    )

}

export default CategoriaProductosEditar;