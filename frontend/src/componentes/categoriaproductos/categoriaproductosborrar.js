import axios from 'axios';
import Swal from 'sweetalert2';
//import {useNavigate} from 'react-router'
function CategoriaProductosBorrar(id)
{    
    //const navegar = useNavigate()

    function categoriaproductosRefrescar()
    {
        //navegar('/')
        window.location.href="/categoriaproductoslistar";
    }

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
        },
            buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({title: '¿Realmente desea eliminar este registro?',
        text: "No es posible revertir este cambio",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
        }).then((result) => {
        if (result.isConfirmed) {
        
        axios.delete(`/api/categoriaproductos/borrar/${id}`)
        .then(() => this.setState({ status: 'Borrado Exitoso' }));
        
        categoriaproductosRefrescar()

        swalWithBootstrapButtons.fire(
            '¡Operación Exitosa!',
            'El registro ha sido eliminado exitosamente',
            'success'
        )


    } else if (
        result.dismiss === Swal.DismissReason.cancel
        ) {
        swalWithBootstrapButtons.fire(
            '¡ERROR!',
            'El registro no pudo ser eliminado',
            'error'
            )
        }
    })
    
}

export default CategoriaProductosBorrar;