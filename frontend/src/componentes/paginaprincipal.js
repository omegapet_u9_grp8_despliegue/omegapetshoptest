import React, { useEffect } from 'react';
import Encabezado from './encabezado'
import MenuLateral from './menulateral';
import PedidosListar from '../componentes/pedidos/pedidoslistar';
import './paginaprincipal.css';

function PaginaPrincipal()
{
    return(
        <div className="sidebar-mini fixed">

            <div className="wrapper">
                <Encabezado />
                <MenuLateral />
                {/* <PedidosListar /> */}
            </div>

        </div>
    )
}

export default PaginaPrincipal;


/*
function Ejemplo() 
{
    useEffect(function ()
    {
        console.log('render!')
    })

    return (<span>This is a useEffect example</span>)
}

export default Ejemplo;
*/