import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function UsuariosEditar()
{
    const parametros = useParams()
    const[nombre, setNombre] = useState('')
    const[email, setEmail] = useState('')
    const[password, setPassword] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/usuarios/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/usuarios/cargar/${parametros.id}`
        /*{
            headers:{'x-auth-token':''}
        }*/
        ).then(res => {
        //axios.post(`/api/usuarios/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataUsuarios = res.data[0]
        setNombre(dataUsuarios.nombre)
        setEmail(dataUsuarios.email)
        setPassword(dataUsuarios.password)
        setActivo(dataUsuarios.activo)
        })
    }, [])

    function usuariosActualizar()
    {
        const usuarioactualizar = {
            id: parametros.id,
            nombre: nombre,
            email: email,
            password: password,
            activo: activo
        }

        console.log(usuarioactualizar)
        axios.post(`/api/usuarios/editar/${parametros.id}`,usuarioactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/usuarioslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function usuariosRegresar()
    {
        //window.location.href="/";
        navigate('/usuarioslistar')
    }


    return(
    
        <div className="container mt-5">
        <h4>Editar Usuarios</h4>
        <div className="row">
            <div class="col-sm-12 table-responsive">
                <table className="table table-inverse">
                    <div className="col-sm-12">
                        <form>
                            <div className="form-group">
                                <label htmlFor="nombre">Nombre</label>
                                <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email</label>
                                <input type="text" className="form-control" id="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}></input>
                            </div>
                            {/* <div className="form-group">
                                <label htmlFor="password">Password</label>
                                <input type="text" className="form-control" id="password" value={password} onChange={(e)=>{setPassword(e.target.value)}}></input>
                            </div> */}
                            <div className="form-group">
                                <label htmlFor="activo">Estado</label>
                                <input type="text" className="form-control" id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                            </div>
                            <button type="button" className="btn btn-info" onClick={usuariosRegresar}>Regresar</button>
                            <button type="button" className="btn btn-success" onClick={usuariosActualizar}>Actualizar</button>
                        </form>
                    </div>
                </table>
            </div>
        </div>
    </div>  
   
    )

}

export default UsuariosEditar;