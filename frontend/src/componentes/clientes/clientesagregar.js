import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';
import '../text_white.css';

function ClientesAgregar()
{
    const[id, setId] = useState('')
    const[id_tipodocumento, setIdTipoDocumento] = useState('')
    const[nombre, setNombre] = useState('')
    const[telefono, setTelefono] = useState('')
    const[direccion, setDireccion] = useState('')
    const[email, setEmail] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function clientesInsertar()
    {

        const clienteinsertar = {
            id: id,
            id_tipodocumento: id_tipodocumento,
            nombre: nombre,
            telefono: telefono,
            direccion: direccion,
            email: email,
            activo: activo
        }

        console.log(clienteinsertar)

        axios.post(`/api/clientes/agregar`,clienteinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/clienteslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function clientesRegresar()
    {
        //window.location.href="/";
        navigate('/clienteslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Cliente</h4>
            <div className="row">
            <div class="col-sm-12 table-responsive">
            <table className="table table-inverse">
                <div className="col-md-12">
                    <div className="mb-3">
                        <label htmlFor="id" className="form-label">Num Documento</label>
                        <input type="text" className="form-control" id="id" value={id} onChange={(e) => {setId(e.target.value)}}></input>
                    </div>     
                    <div className="mb-3">
                        <label htmlFor="id_tipodocumento" className="form-label">Id Tipo Documento</label>
                        <input type="text" className="form-control" id="id_tipodocumento" value={id_tipodocumento} onChange={(e) => {setIdTipoDocumento(e.target.value)}}></input>
                    </div>           
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>  
                    <div className="mb-3">
                        <label htmlFor="telefono" className="form-label">Telefono</label>
                        <input type="text" className="form-control" id="telefono" value={telefono} onChange={(e) => {setTelefono(e.target.value)}}></input>
                    </div>  
                    <div className="mb-3">
                        <label htmlFor="direccion" className="form-label">Direccion</label>
                        <input type="text" className="form-control" id="direccion" value={direccion} onChange={(e) => {setDireccion(e.target.value)}}></input>
                    </div>  
                    <div className="mb-3">
                        <label htmlFor="email" className="form-label">Email</label>
                        <input type="text" className="form-control" id="email" value={email} onChange={(e) => {setEmail(e.target.value)}}></input>
                    </div>           
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Estado</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={clientesRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={clientesInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
                </table>
            </div>
            </div>
        </div>
    )

}

export default ClientesAgregar;