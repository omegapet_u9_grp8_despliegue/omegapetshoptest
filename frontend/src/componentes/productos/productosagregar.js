import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';
import '../text_white.css';

function ProductosAgregar()
{
    const[id_categoria, setIdCategoria] = useState('')
    const[nombre, setNombre] = useState('')
    const[descripcion, setDescripcion] = useState('') 
    const[precio, setPrecio] = useState('') 
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function productosInsertar()
    {

        const productoinsertar = {
            id: uniquid(),
            id_categoria: id_categoria,
            nombre: nombre,
            descripcion: descripcion,  
            precio: precio,
            activo: activo
        }

        console.log(productoinsertar)

        axios.post(`/api/productos/agregar`,productoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/productoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function productosRegresar()
    {
        //window.location.href="/";
        navigate('/productoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Producto</h4>
            <div className="row">
            <div class="col-sm-12 table-responsive">
            <table className="table table-inverse">
                <div className="col-md-12">
                    {/* <div className="mb-3">
                        <label htmlFor="id_categoria" className="form-label">Id Categoria</label>
                        <input type="text" className="form-control" id="id_categoria" value={id_categoria} onChange={(e) => {setIdCategoria(e.target.value)}}></input>
                    </div>                     */}
                     <div className="form-group" onChange={(e)=>{setIdCategoria(e.target.value)}}>
                        <label htmlFor="id_categoria">Id Categoria</label>
                        <select id="id_categoria">
                            <option value="" selected>Elige una Opción</option>
                            <option value="1">1.Alimento</option>
                            <option value="2">2.Accesorio</option>
                            <option value="3">3.Juguete</option>
                            <option value="4">4.Limpieza</option>
                        </select>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="descripcion" className="form-label">Descripcion</label>
                        <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => {setDescripcion(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="precio" className="form-label">Precio</label>
                        <input type="text" className="form-control" id="precio" value={precio} onChange={(e) => {setPrecio(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Estado</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={productosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={productosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
                </table>
            </div>
            </div>
        </div>
    )

}

export default ProductosAgregar;