import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';
import ProductosCombo from '../productoscombo';

function ProductosEditar()
{
    const parametros = useParams()
    const[id_categoria, setIdCategoria] = useState('')
    const[nombre, setNombre] = useState('')
    const[descripcion, setDescripcion] = useState('') 
    const[precio, setPrecio] = useState('') 
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/productos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/productos/cargar/${parametros.id}`
        /*{
            headers:{'x-auth-token':''}
        }*/
        ).then(res => {
        //axios.post(`/api/productos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataProductos = res.data[0]
        setIdCategoria(dataProductos.id_categoria)
        setNombre(dataProductos.nombre)
        setDescripcion(dataProductos.descripcion)
        setPrecio(dataProductos.precio)
        setActivo(dataProductos.activo)
        })
    }, [])

    function productosActualizar()
    {
        const productoactualizar = {
            id: parametros.id,
            id_categoria: id_categoria,
            nombre: nombre,
            descripcion: descripcion,  
            precio: precio,      
            activo: activo
        }

        console.log(productoactualizar)
        axios.post(`/api/productos/editar/${parametros.id}`,productoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/productoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function productosRegresar()
    {
        //window.location.href="/";
        navigate('/productoslistar')

    }

    

    return(

    <div className="container mt-5">
        <h4>Editar Producto</h4>
        <div className="row">
            <div class="col-sm-12 table-responsive">
                <table className="table table-inverse">
                    <div className="col-sm-12">

                        {/* <h5 className="mt-5">PRODUCTO</h5> */}
                        <form>
                            {/* <div className="form-group">
                                <label htmlFor="id_categoria">Id Categoria</label>
                                <input type="text" className="form-control"id="id_categoria" value={id_categoria} onChange={(e)=>{setIdCategoria(e.target.value)}}></input>
                            </div> */}
                           <div className="form-group" onChange={(e)=>{setIdCategoria(e.target.value)}}>
                                <label htmlFor="id_categoria">Id Categoria</label>
                                <select id="id_categoria">
                                    <option value="" selected>Elige una Opción</option>
                                    <option value="1">1.Alimento</option>
                                    <option value="2">2.Accesorio</option>
                                    <option value="3">3.Juguete</option>
                                    <option value="4">4.Limpieza</option>
                                </select>
                            </div>

                            <div className="form-group">
                                <label htmlFor="nombre">Nombre</label>
                                <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e)=>{setNombre(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="descripcion">Descripcion</label>
                                <input type="text" className="form-control"id="id_descripcion" value={descripcion} onChange={(e)=>{setDescripcion(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="precio">Precio</label>
                                <input type="text" className="form-control" id="precio" value={precio} onChange={(e)=>{setPrecio(e.target.value)}}></input>
                            </div>
                            <div className="form-group">
                                <label htmlFor="activo">Estado</label>
                                <input type="text" className="form-control" id="activo" value={activo} onChange={(e)=>{setActivo(e.target.value)}}></input>
                            </div>
                            <button type="button" className="btn btn-info" onClick={productosRegresar}>Regresar</button>
                            <button type="button" className="btn btn-success" onClick={productosActualizar}>Actualizar</button>
                        </form>
                    </div>
                </table>
            </div>
        </div>
    </div>
)

}

export default ProductosEditar;