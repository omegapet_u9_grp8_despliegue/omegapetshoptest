import {Link} from 'react-router-dom';
import './text_white.css';

function MenuLateral()
{

    return(
                <aside className="main-sidebar hidden-print">
                 
                <section className="sidebar" id="sidebar-scroll">
                
                    <ul className="sidebar-menu">
                        <li className="nav-level">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Principal</li>
                        <div className= 'gray3'>
                        <li className="treeview">
                        {/* href="#!" */}
                            <a className="waves-effect waves-dark" href="paginaprincipal">
                                <i className="icon-speedometer"></i><span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inicio</span>
                            </a>                
                        </li>

                        <li className="nav-level">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Productos</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="/productoslistar"><i className="icon-briefcase"></i>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Productos</span><i className="icon-arrow-down"></i></a>
                        </li>

                        <li className="treeview"><a className="waves-effect waves-dark" href="/categoriaproductoslistar"><i className="icon-briefcase"></i>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Categoría Productos</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i> Listar Clientes</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i> Agregar Clientes</a></li>
                            </ul>
                        </li>

                        <li className="nav-level">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pedidos</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="/pedidoslistar"><i className="icon-briefcase"></i>
                        <span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pedidos</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i> Listar Pedidos</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i> Agregar Pedidos</a></li>
                            </ul>
                        </li> 

                        <li className="treeview"><a className="waves-effect waves-dark" href="/detallepedidoslistar"><i className="icon-briefcase"></i>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Detalle Pedidos</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i> Listar Clientes</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i> Agregar Clientes</a></li>
                            
                            </ul>
                            
                        </li>

                        <li className="nav-level">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Facturas</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="/facturaslistar"><i className="icon-briefcase"></i>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Facturas</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i> Listar Clientes</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i> Agregar Clientes</a></li>
                            </ul>
                        </li>

                        <li className="nav-level">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clientes</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="/clienteslistar"><i className="icon-briefcase"></i>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clientes</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i> Listar Clientes</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i> Agregar Clientes</a></li>
                            </ul>
                        </li>

                        <li className="nav-level">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usuarios</li>
                        <li className="treeview"><a className="waves-effect waves-dark" href="/usuarioslistar"><i className="icon-briefcase"></i>   
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Usuarios</span><i className="icon-arrow-down"></i></a>
                            <ul className="treeview-menu">
                                <li><a className="waves-effect waves-dark" href="accordion.html"><i className="icon-arrow-right"></i> Listar Clientes</a></li>
                                <li><a className="waves-effect waves-dark" href="button.html"><i className="icon-arrow-right"></i> Agregar Clientes</a></li>
                            </ul>
                        </li>

                        </div>
                    </ul>
                    
                </section>
                
            </aside> 

    )

}

export default MenuLateral;