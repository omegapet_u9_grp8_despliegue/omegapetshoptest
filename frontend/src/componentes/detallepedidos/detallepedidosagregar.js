import axios from 'axios';
import uniquid from 'uniqid';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';
import '../text_white.css';

function DetallePedidosAgregar()
{
    const[id_pedido, setIdPedido] = useState('')
    const[id_producto, setIdProducto] = useState('')
    const[cantidad, setCantidad] = useState('') 
    const[valor, setValor] = useState('') 
    // const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function detallePedidosInsertar()
    {

        const detallepedidoinsertar = {
            id: uniquid(),
            id_pedido: id_pedido,
            id_producto: id_producto,
            cantidad: cantidad,  
            valor: valor,
            // activo: activo
        }

        console.log(detallepedidoinsertar)

        axios.post(`/api/detallepedidos/agregar`,detallepedidoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/detallepedidoslistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function detallePedidosRegresar()
    {
        //window.location.href="/";
        navigate('/detallepedidoslistar')
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Detalle Pedido</h4>
            <div className="row">
            <div class="col-sm-12 table-responsive">
            <table className="table table-inverse">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="id_pedido" className="form-label">Id Pedido</label>
                        <input type="text" className="form-control" id="id_pedido" value={id_pedido} onChange={(e) => {setIdPedido(e.target.value)}}></input>
                    </div>                    
                    <div className="mb-3">
                        <label htmlFor="id_producto" className="form-label">Id Producto</label>
                        <input type="text" className="form-control" id="id_producto" value={id_producto} onChange={(e) => {setIdProducto(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="cantidad" className="form-label">Cantidad</label>
                        <input type="text" className="form-control" id="cantidad" value={cantidad} onChange={(e) => {setCantidad(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="valor" className="form-label">Valor</label>
                        <input type="text" className="form-control" id="valor" value={valor} onChange={(e) => {setValor(e.target.value)}}></input>
                    </div>
                    {/* <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                 */}
                    <div className="mb-12">
                        <button type="button" onClick={detallePedidosRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={detallePedidosInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
                </table>
            </div>
            </div>
        </div>
    )

}

export default DetallePedidosAgregar;