const express = require('express');
const app = express();
const cors = require("Cors");

//establezco la conexion
const miconexion = require('./conexion');

//habilitar cors
app.use(cors());

//importo el body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//importo las rutas
const rutas = require('./routers/routers');
app.use('/api', rutas);

//peticion de prueba con el metodo .get
app.get('/', (req,res) =>{
	res.send("Servidor backend corriendo OK!!!");
})

//iniciando servidor en el puerto 5000
app.listen(5000, function()
{
	console.log("Mi servidor funciona en el puerto 5000 - http://localhost:5000");
})

