const express = require('express');
const router = express.Router();
const modeloPedidos = require('../models/model_pedidos');

router.get('/listar', (req, res) => {
    modeloPedidos.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloPedidos.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar', (req, res) => {

    const nuevoPedido = new modeloPedidos({
        id : req.body.id,
        id_cliente : req.body.id_cliente,
        fecha : req.body.fecha,
        activo : req.body.activo
    }) 

    nuevoPedido.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    })
});

router.post('/editar/:id', (req, res) => {
    modeloPedidos.findOneAndUpdate({id:req.params.id},
        {
            id_cliente : req.body.id_cliente,
            fecha : req.body.fecha,
            activo : req.body.activo
        }, (err) =>
        {
            if(!err)
            {
            res.send("El registro se editó exitosamente");
            }   
            else
            {   
            res.send(err);
            }
        })
});

router.delete('/borrar/:id', (req, res) => {
    modeloPedidos.findOneAndDelete({id:req.params.id}, (err) =>
        {
            if(!err)
            {
            res.send("El registro se borró exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});

module.exports = router;