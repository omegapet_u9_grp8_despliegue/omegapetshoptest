const express = require('express');
const router = express.Router();
const modeloFacturas = require('../models/model_facturas');

router.get('/listar', (req, res) => {
    modeloFacturas.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloFacturas.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar', (req, res) => {

    const nuevaFactura = new modeloFacturas({
        id: req.body.id,
        id_pedido: req.body.id_pedido,
        activo: req.body.activo
    }); 

    nuevaFactura.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    })
});

router.post('/editar/:id', (req, res) => {
    modeloFacturas.findOneAndUpdate({id:req.params.id},
        {
            id_pedido: req.body.id_pedido,
            activo: req.body.activo
        }, (err) =>
        {
            if(!err)
            {
            res.send("El registro se editó exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});

router.delete('/borrar/:id', (req, res) => {
    modeloFacturas.findOneAndDelete({id:req.params.id}, (err) =>
        {
            if(!err)
            {
            res.send("El registro se borró exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});



module.exports = router;