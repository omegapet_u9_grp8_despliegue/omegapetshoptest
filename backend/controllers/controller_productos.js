const express = require('express');
const router = express.Router();
const modeloProductos = require('../models/model_productos');

router.get('/listar', (req, res) => {
    modeloProductos.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloProductos.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar', (req, res) => {

    const nuevoProducto = new modeloProductos({
        id: req.body.id,
        id_categoria: req.body.id_categoria,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        precio: req.body.precio,
        activo: req.body.activo
    }) 

    nuevoProducto.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    })
});

router.post('/editar/:id', (req, res) => {
    modeloProductos.findOneAndUpdate({id:req.params.id},
        {
            id_categoria: req.body.id_categoria,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            precio: req.body.precio,
            activo: req.body.activo
        }, (err) =>
        {
            if(!err)
            {
            res.send("El registro se editó exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});

router.delete('/borrar/:id', (req, res) => {
    modeloProductos.findOneAndDelete({id:req.params.id}, (err) =>
        {
            if(!err)
            {
            res.send("El registro se borró exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});

module.exports = router;