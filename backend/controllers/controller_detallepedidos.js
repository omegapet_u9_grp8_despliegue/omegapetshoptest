const express = require('express');
const router = express.Router();
const modeloDetallePedidos = require('../models/model_detallepedidos');

router.get('/listar', (req, res) => {
    modeloDetallePedidos.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloDetallePedidos.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar', (req, res) => {

    const nuevoDetallePedido = new modeloDetallePedidos({
        id: req.body.id,
        id_pedido: req.body.id_pedido,
        id_producto: req.body.id_producto,
        cantidad: req.body.cantidad,
        valor: req.body.valor
    }); 

    nuevoDetallePedido.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    })
});

router.post('/editar/:id', (req, res) => {
    modeloDetallePedidos.findOneAndUpdate({id:req.params.id},
        {
            id_pedido: req.body.id_pedido,
            id_producto: req.body.id_producto,
            cantidad: req.body.cantidad,
            valor: req.body.valor
        }, (err) =>
        {
            if(!err)
            {
            res.send("El registro se editó exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});

router.delete('/borrar/:id', (req, res) => {
    modeloDetallePedidos.findOneAndDelete({id:req.params.id}, (err) =>
        {
            if(!err)
            {
            res.send("El registro se borró exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});



module.exports = router;