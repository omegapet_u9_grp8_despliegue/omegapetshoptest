const express = require('express');
const router = express.Router();
const modeloClientes = require('../models/model_clientes');
const modeloPedidos = require('../models/model_pedidos');

//const miconexion = require('../conexion');

router.get('/pedidosclientes', (req, res) => {
    
modeloClientes.aggregate([    
    {
        $lookup: {
        localField: "id",             //id del cliente
        from: "pedidos",              //con qué colección hará el cruce
        foreignField: "id_cliente",   //clave foránea
        as: "pedidos_clientes"         //alias
        }
    },
    { 
        $unwind: "$pedidos_clientes" 
    }
    ])
    .then((result)=>{res.send(result); console.log(result)}) 
    .catch((error)=>{res.send(result); console.log(error)}); 

});

router.get('/pedidosclientes/:id', (req, res) => {
var dataClientes = []; 
modeloClientes.find({id:req.params.id}).then(data => { 
    console.log("Datos del Cliente:"); 
    console.log(data); 
    data.map((d, k) => {dataClientes.push(d.id);})
    modeloPedidos.find({id_cliente: { $in: dataClientes}})
    .then(data => {
        console.log("Pedidos del cliente:");
        console.log(data); 
        res.send(data);
    })
    .catch((error)=>{console.log(error)});
})
});

module.exports = router;