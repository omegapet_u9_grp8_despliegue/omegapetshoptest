const express = require('express');
const router = express.Router();
const modeloCategoriaProductos = require('../models/model_categoriaproductos');

router.get('/listar', (req, res) => {
    modeloCategoriaProductos.find({}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloCategoriaProductos.find({id:req.params.id}, function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar', (req, res) => {

    const nuevaCategoriaProducto = new modeloCategoriaProductos({
        id: req.body.id,
        nombre : req.body.nombre,
        activo : req.body.activo
    }); 

    nuevaCategoriaProducto.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    })
});

router.post('/editar/:id', (req, res) => {
    modeloCategoriaProductos.findOneAndUpdate({id:req.params.id},
        {
            nombre : req.body.nombre,
            activo : req.body.activo
        }, (err) =>
        {
            if(!err)
            {
            res.send("El registro se editó exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});

router.delete('/borrar/:id', (req, res) => {
    modeloCategoriaProductos.findOneAndDelete({id:req.params.id}, (err) =>
        {
            if(!err)
            {
            res.send("El registro se borró exitosamente");
            }   
            else
            {   
            res.send(err.stack);
            }
        })
});



module.exports = router;