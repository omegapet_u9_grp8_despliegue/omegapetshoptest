//Rutas para crear usuarios

const express = require("express");
const router = express.Router();
const usuarioController = require("../controllers/controller_usuarios");
const modeloUsuarios = require("../models/model_usuarios");
const { check } = require("express-validator");

// Crea un usuario

//api/usuarios
router.post(
  "/",
  [
    check("nombre", "El nombre es obligatorio").not().isEmpty(),
    check("email", "Agrega un email válido").isEmail(),
    check("password", "El password debe ser mínimo de 6 caracteres").isLength({
      min: 6,
    }),
  ],
  usuarioController.crearUsuario
);

router.get('/listar', (req, res) => {
  modeloUsuarios.find({}, function(docs,err)
  {
      if(!err)
      {
          res.send(docs);
      }
      else
      {
          res.send(err);
      }
  })
});

router.get('/cargar/:_id', (req, res) => {
  modeloUsuarios.find({id:req.params.id}, function(docs,err)
  {
      if(!err)
      {
          res.send(docs);
      }
      else
      {
          res.send(err);
      }
  })
});

router.post('/editar/:_id', (req, res) => {
  modeloUsuarios.findOneAndUpdate({id:req.params.id},
      {
          nombre: req.body.nombre,
          email: req.body.email,
          activo: req.body.activo
      }, (err) =>
      {
          if(!err)
          {
          res.send("El registro se editó exitosamente");
          }   
          else
          {   
          res.send(err.stack);
          }
      })
});

router.delete('/borrar/:_id', (req, res) => {
  modeloUsuarios.findOneAndDelete({id:req.params.id}, (err) =>
      {
          if(!err)
          {
          res.send("El registro se borró exitosamente");
          }   
          else
          {   
          res.send(err.stack);
          }
      })
});

module.exports = router;
