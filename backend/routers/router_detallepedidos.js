const express = require('express');
const router = express.Router();
const controladorDetallePedidos = require('../controllers/controller_detallepedidos');
const auth = require('../middleware/auth');

router.get('/listar', controladorDetallePedidos);
router.get('/cargar/:id', controladorDetallePedidos); 
router.post('/agregar', controladorDetallePedidos);
router.post('/editar/:id', controladorDetallePedidos);
router.delete('/borrar/:id', controladorDetallePedidos);

module.exports = router;