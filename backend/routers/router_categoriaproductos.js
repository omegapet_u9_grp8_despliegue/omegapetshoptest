const express = require('express');
const router = express.Router();
const controladorCategoriaProductos = require('../controllers/controller_categoriaproductos');
const auth = require('../middleware/auth');

router.get('/listar', controladorCategoriaProductos);
router.get('/cargar/:id', controladorCategoriaProductos); 
router.post('/agregar', controladorCategoriaProductos);
router.post('/editar/:id', controladorCategoriaProductos);
router.delete('/borrar/:id', controladorCategoriaProductos);

module.exports = router;