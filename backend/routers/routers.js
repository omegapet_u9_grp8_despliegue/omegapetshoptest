const express = require('express');
const router = express.Router();

const rutaProductos = require('./router_productos');
router.use('/productos', rutaProductos);

const rutaClientes = require('./router_clientes');
router.use('/clientes', rutaClientes);

const rutaPedidos = require('./router_pedidos');
router.use('/pedidos', rutaPedidos);

const rutaDetallePedidos = require('./router_detallepedidos');
router.use('/detallepedidos', rutaDetallePedidos);

const rutaFacturas = require('./router_facturas');
router.use('/facturas', rutaFacturas);

const rutaCategoriaProductos = require('./router_categoriaproductos');
router.use('/categoriaproductos', rutaCategoriaProductos);

//consultas
const rutaConsultas = require('./router_consultas');
router.use('/consultas', rutaConsultas);

//autentificación
const rutaUsuarios = require('./router_usuarios');
router.use('/usuarios', rutaUsuarios);

const rutaAuth = require('./router_auth');
router.use('/auth', rutaAuth);

module.exports = router;