const express = require('express');
const router = express.Router();
const controladorPedidos = require('../controllers/controller_pedidos');
const auth = require('../middleware/auth');



router.get('/listar',auth, controladorPedidos);
router.get('/cargar/:id', controladorPedidos);
router.post('/agregar', controladorPedidos);
router.post('/editar/:id', controladorPedidos);
router.delete('/borrar/:id', controladorPedidos);

module.exports = router;