const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const controladorpedidosclientes = require('../controllers/consultar_pedidos_clientes');
router.get('/pedidosclientes', controladorpedidosclientes);
router.get('/pedidosclientes/:id', controladorpedidosclientes); 


module.exports = router;