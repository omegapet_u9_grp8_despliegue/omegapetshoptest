const express = require('express');
const router = express.Router();
const controladorFacturas = require('../controllers/controller_facturas');
const auth = require('../middleware/auth');

router.get('/listar', controladorFacturas);
router.get('/cargar/:id', controladorFacturas); 
router.post('/agregar', controladorFacturas);
router.post('/editar/:id', controladorFacturas);
router.delete('/borrar/:id', controladorFacturas);

module.exports = router;