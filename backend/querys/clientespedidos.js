const modeloClientes = require('../models/model_clientes');
const modeloPedidos = require('../models/model_pedidos');

const miconexion = require('../conexion');
/*
//RELACION ENTRE COLECCIONES UTILIZANDO EL METODO AGGREGATE DE MONGODB
//INICIAR CON EL COMANDO node clientespedidos.js
modeloClientes.aggregate([    
    {
        $lookup: {
        localField: "id",             //id del cliente
        from: "pedidos",              //con qué colección hará el cruce
        foreignField: "id_cliente",   //clave foránea
        as: "pedidos_clientes"         //alias
        }
    },

//unwind configura la salida, en relación a qué 
//"nombre" tendrá la subcolección en el documento que se generará
    { 
        $unwind: "$pedidos_clientes" 
    }
    ])
    .then((result)=>{/*res.send(result);*/ /*console.log(result)}) 
    //si hay resultado imprime el documento
    .catch((error)=>{/*res.send(result);*/ /*console.log(error)}); */
    //o sino capturamos el error 

//RELACION ENTRE COLECCIONES UTILIZANDO METODOS BASICOS DE MONGODB Y ARREGLOS

//se crea un arreglo.
var dataClientes = []; 
 //se llama al modeloClientes y se busca por id(cedula del cliente)
 //data es una variable en la que se hará una función dinámica...
modeloClientes.find({id: "123456789"}).then(data => { 
    //mensaje de referencia
    console.log("Datos del Cliente:"); 
    //data de salida a mostrar
    console.log(data); 
    //mapeando la data, creamos 2 parámetros
    //luego llamamos a la variable dataClientes     
    //y con .push recibimos el id en el parámetro d
    data.map((d, k) => {dataClientes.push(d.id);})
    //una vez con el mapeado, relacionamos con el modeloPedidos
    //para ello primero con .find traemos el atributo id_cliente
    //y le decimos con el método $in 
    //que seleccione todo lo que se encuentre en dataCLientes
    modeloPedidos.find({id_cliente: { $in: dataClientes}})
    //continuamos la función con el método .then 
    //y si cumple la condición le decimos que imprima los
    //pedidos del cliente, con esto completamos la consulta realizada
    .then(data => {
        console.log("Pedidos del cliente:");
        console.log(data); 
    })
    .catch((error)=>{console.log(error)}); 
});

