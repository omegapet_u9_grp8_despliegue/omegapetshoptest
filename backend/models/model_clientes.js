const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaClientes = new miesquema({
    id: String,
    id_tipodocumento:String,
    nombre: String,
    telefono: String,
    direccion: String,
    email: String,
    activo: Boolean
})

const modeloClientes = mongoose.model('clientes',esquemaClientes);

module.exports= modeloClientes; 

/*module.exports={modeloCliente, } //metodo para especificar exclusivamente lo que se quiere o no exportar*/