const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaProductos = new miesquema({
    id: String,
    id_categoria: String,
    nombre: String,
    descripcion: String,
    precio: Number,
    activo: Boolean
})

const modeloProductos = mongoose.model('productos',esquemaProductos);

module.exports= modeloProductos; 