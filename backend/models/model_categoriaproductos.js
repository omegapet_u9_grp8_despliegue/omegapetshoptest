const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const esquemaCategoriaProductos = new miesquema({
    id : String,
    nombre : String,
    activo : Boolean
})

const modeloCategoriaProductos = mongoose.model('categoriaproductos',esquemaCategoriaProductos);

module.exports= modeloCategoriaProductos; 